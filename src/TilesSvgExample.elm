module TilesSvgExample exposing (drawFrame, initGarden)

import Dict
import GenGarden
import List.Extra
import Maybe
import Random


settingRows =
    "Number of Columns and Rows"


{-| Each Slider record will generate a slider to allow you to change a
-- draw setting at run time. You can have up to 10 sliders.
-}
mySliders : List GenGarden.Slider
mySliders =
    [ { label = settingRows
      , max = 60
      , min = 1
      , step = 1
      , value = 10
      }
    ]


{-| Redraw the image area, called every tick
-}
drawFrame : Dict.Dict String Float -> Float -> List (GenGarden.Drawing msg)
drawFrame settings ticks =
    let
        numCols =
            Dict.get settingRows settings
                |> Maybe.withDefault 10
                |> round

        width =
            200 / toFloat numCols

        -- set up tiles from 0,0 to numCols,numCols
        tilePos =
            List.range 0 (numCols * numCols)
                |> List.map
                    (\tileIndex -> ( modBy numCols tileIndex, tileIndex // numCols ))

        tilePosBool =
            round ticks
                |> GenGarden.listOfRandomInts (numCols * numCols) 1
                |> List.map (\x -> modBy 2 x == 1)
                |> List.Extra.zip tilePos

        tile : ( ( Int, Int ), Bool ) -> List (GenGarden.Drawing msg)
        tile ( ( x, y ), b ) =
            drawTile ( x, y ) b width "purple" settings
    in
    List.concatMap
        (\x -> tile x)
        tilePosBool


{-| Redraw the image area, called every tick
-}
drawTile :
    ( Int, Int )
    -> Bool
    -> Float
    -> String
    -> Dict.Dict String Float
    -> List (GenGarden.Drawing msg)
drawTile ( tx, ty ) flip width color settings =
    let
        x =
            toFloat tx

        y =
            toFloat ty

        left =
            x * width - 100

        top =
            y * width - 100

        half =
            width / 2

        leftCenter =
            ( left, top + half )

        rightCenter =
            ( left + width, top + half )

        topCenter =
            ( left + half, top )

        bottomCenter =
            ( left + half, top + width )

        ( ( start, end ), ( start2, end2 ) ) =
            if flip then
                ( ( leftCenter, topCenter ), ( rightCenter, bottomCenter ) )

            else
                ( ( rightCenter, topCenter ), ( leftCenter, bottomCenter ) )
    in
    [ GenGarden.line start end color [] []
    , GenGarden.line start2 end2 color [] []
    ]


fWidth =
    200


fHeight =
    200


initGarden : GenGarden.Model
initGarden =
    GenGarden.init ( fWidth, fHeight ) 1000 mySliders
