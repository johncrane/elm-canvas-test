module Main exposing (init, main, view)

-- import SimpleExample as Example
-- import TilesExample as Example
-- import SimpleSvgExample as Example

import Browser
import Canvas
import Color
import Dict
import GenGarden
import Html
import Html.Attributes exposing (style)
import List.Extra
import Maybe
import Random
import TilesSvgExample as Example


{-| Choose Svg or Canvas
-}
viewFunction =
    GenGarden.viewSvg



-- viewFunction =
--    GenGarden.viewCanvas


type alias Model =
    { garden : GenGarden.Model
    }


type Msg
    = GardenMsg GenGarden.Msg


main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { garden = Example.initGarden
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ GenGarden.subscriptions model.garden
            |> Sub.map GardenMsg
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GardenMsg gMsg ->
            let
                ( gModel, cmd ) =
                    GenGarden.update gMsg model.garden
            in
            ( { model | garden = gModel }
            , Cmd.batch
                [ Cmd.map GardenMsg cmd
                ]
            )


view : Model -> Html.Html Msg
view model =
    Html.div []
        (viewFunction Example.drawFrame model.garden
            |> List.map (Html.map GardenMsg)
        )
