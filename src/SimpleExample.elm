module SimpleExample exposing (drawFrame, initGarden)

import Canvas
import Color
import Dict
import GenGarden


{-| Each Slider record will generate a slider to allow you to change a
-- draw setting at run time. You can have up to 10 sliders.
-}
mySliders : List GenGarden.Slider
mySliders =
    [ { label = "Hue"
      , max = 1.0
      , min = 0.0
      , step = 0.025
      , value = 0.5
      }
    ]


height : number
height =
    400


width : number
width =
    720


{-| Redraw the image area, pass this to GenGarden.view
-}
drawFrame : Dict.Dict String Float -> Float -> List Canvas.Renderable
drawFrame settings ticks =
    let
        hue =
            Dict.get "Hue" settings |> Maybe.withDefault 0.5
    in
    [ Canvas.shapes [ Canvas.fill (Color.hsla hue 0.5 0.5 1.0) ]
        [ Canvas.rect
            ( 100, 50 )
            (width / 2)
            (height / 2)
        ]
    ]


initGarden : GenGarden.Model
initGarden =
    GenGarden.init ( width, height ) 1000 mySliders
