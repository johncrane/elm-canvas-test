This project is set up to build Elm web apps using the elm-canvas web component. 

Build command

`elm make src/*.elm --output=main.js`

Test command

`elm-live src/Main.elm --open --start-page test.html`